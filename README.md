# tg-tipbot

Telegram tip bot.

## Setup

```
# initialize new wallet and retain seed
docker run --rm -it --name wow-wallet-init \
  -v $(pwd)/data:/root \
  lalanza808/wownero \
  wownero-wallet-cli \
    --daemon-address https://node.suchwow.xyz:443 \
    --generate-new-wallet /root/wow \
    --password zzzzzz \

# setup rpc process
docker run --rm -d --name wow-wallet \
  -v $(pwd)/data:/root \
  -p 8888:8888 \
  lalanza808/wownero \
  wownero-wallet-rpc \
    --daemon-address https://node.suchwow.xyz:443 \
    --wallet-file /root/wow \
    --password zzzzzz \
    --rpc-bind-port 8888 \
    --rpc-bind-ip 0.0.0.0 \
    --confirm-external-bind \
    --rpc-login xxxx:yyyy \
    --log-file /root/rpc.log

# install python dependencies
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

# setup secrets in config file outside of git
cp tipbot/config.example.py tipbot/config.py
vim !$
```

## Usage

There are 2 modules in this repo:

- `tipbot` - the Telegram bot process
- `admin` - management commands

To use them, ensure you're in the virtualenv and run `python3 -m tipbot` or `python3 -m admin`

## Development

To add more commands, create a new Python file under [tipbot/commands](tipbot/commands); copy another one for an example. Add another import for the command function in [tipbot/commands/__init__.py](tipbot/commands/__init__.py) and add the command to the `all_commands` dictionary in the same file.
