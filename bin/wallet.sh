#!/bin/bash

export $(cat .env)

if [ ! -d "$WALLET_PATH" ]; then
  # initialize new wow wallet and retain seed
  mkdir -p $WALLET_PATH
  docker run --rm -it --name tgbot-wallet-init \
    -v $WALLET_PATH:/root \
    lalanza808/wownero:latest \
    wownero-wallet-cli \
      --daemon-address $DAEMON_URI \
      --generate-new-wallet /root/wow \
      --password $WALLET_PASS \
      --trusted-daemon
fi

# setup rpc process
docker run -d --name tgbot-wallet \
  --restart=always \
  -v $WALLET_PATH:/root \
  -p 127.0.0.1:9999:9999 \
  lalanza808/wownero:latest \
  wownero-wallet-rpc \
    --daemon-address $DAEMON_URI \
    --wallet-file /root/wow \
    --password $WALLET_PASS \
    --rpc-bind-port 9999 \
    --rpc-bind-ip 0.0.0.0 \
    --confirm-external-bind \
    --rpc-login "$WALLET_RPC_USER:$WALLET_RPC_PASS" \
    --log-file /root/rpc.log \
    --trusted-daemon
