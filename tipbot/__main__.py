import logging
from tipbot import commands, wownero, config
from os import environ
from telegram.ext import Updater, CommandHandler

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

if __name__ == '__main__':
    token = config.TG_TOKEN
    if token:
        logging.info('Starting bot.')
        updater = Updater(token=token, use_context=True)
        dispatcher = updater.dispatcher
        for cmd in commands.all_commands:
            logging.info(f'registering command: {cmd}')
            handler = CommandHandler(cmd, commands.all_commands[cmd]['func'])
            dispatcher.add_handler(handler)
        updater.start_polling()
    else:
        logging.error('No token provided. Quitting!')
        exit(2)
