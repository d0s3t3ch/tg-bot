import logging
from functools import wraps
from tipbot import wownero
from tipbot import db
from tipbot import config
from tipbot.helpers.utils import is_tg_admin


def check_debug(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        msg = args[0].message
        is_admin = is_tg_admin(msg.from_user["id"])
        is_debug = getattr(config, 'DEBUG', True) == True
        if is_debug:
            if not is_admin:
                msg.reply_text('I am in debug mode by my admin. Commands are disabled at the moment. Try again later.')
                return False
        return f(*args, **kwargs)
    return decorated_function

def log_event(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        _d = ''
        if config.DEBUG:
            _d = ' (debug mode on)'
        msg = args[0].message
        logging.info(f'"{f.__name__}" invoked from {msg.from_user["id"]} ({msg.from_user["first_name"]}) - Full command: "{msg.text}"{_d}')
        return f(*args, **kwargs)
    return decorated_function

def wallet_rpc_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        wallet = wownero.Wallet()
        if not wallet.connected:
            logging.error(f'Wallet RPC interface is not available: {args[0].message}')
            args[0].message.reply_text('Wallet RPC interface is not available right now. Try again later.')
            return False
        return f(*args, **kwargs)
    return decorated_function

def registration_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        wallet = wownero.Wallet()
        if not db.User.filter(telegram_id=args[0].message.from_user['id']):
            args[0].message.reply_text('You are not yet registered. Issue the /register command.')
            return False
        return f(*args, **kwargs)
    return decorated_function
