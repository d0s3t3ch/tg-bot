from io import BytesIO

from PIL import Image
from base64 import b64encode
from qrcode import make as qrcode_make
from telegram.error import Unauthorized

from tipbot import config

def is_tg_admin(chat_id):
    if chat_id == config.TG_ADMIN_ID:
        return True
    else:
        return False

def generate_qr(s):
    _address_qr = BytesIO()
    qrcode_make(s).save(_address_qr, format="PNG")
    _address_qr.seek(0)
    return _address_qr

def reply_user(msg, context, text, pm=True, delete=False):
    try:
        if pm:
            msg.from_user.send_message(text)
        else:
            msg.reply_text(text)
    except Unauthorized:
        msg.reply_text(f'You have to initiate a convo with the bot first: https://t.me/{context.bot.username}')
    except:
        msg.reply_text(f'Something borked -_-')

    if delete:
        msg.delete()
