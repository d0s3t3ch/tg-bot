import logging
import requests
from time import time
from tipbot.db import User
from tipbot.helpers.decorators import wallet_rpc_required, log_event, check_debug
from tipbot import wownero
from tipbot import config


@log_event
@check_debug
def mine(update, context):
    # Check that enough arguments were provided
    if len(context.args) < 1:
        update.message.reply_text('Not enough arguments passed. Include hash rate per second to estimate.')
        return False

    # Check that hash rate arg is an actual number
    try:
        hr = float(context.args[0])
    except:
        update.message.reply_text('Invalid hashrate provided')
        return False

    # Run the numbers, tell the user
    try:
        payload = {'jsonrpc':'2.0', 'id':'0', 'method':'getlastblockheader'}
        headers = {'Content-Type':'application/json'}
        r = requests.post(config.DAEMON_URI + '/json_rpc', json=payload, headers=headers, timeout=5)
        r.raise_for_status()
        j = r.json()
        diff = float(j['result']['block_header']['difficulty'])
        lbr = float(j['result']['block_header']['reward'])
        ttb = diff / hr
        update.message.reply_text('The estimated time to find a block with {0:.2f} kH/s at diff {1:.2e} is {2:.2f} days. On average you will earn {3:.2f} WOW per day'.format(
            hr/1e3, diff, ttb/(60*60*24), (lbr/1e11)/(ttb/(60*60*24))
        ))
    except:
        update.message.reply_text('Something b0rked -_-')
        return False

@log_event
@check_debug
def mempool(update, context):
    try:
        payload = {'jsonrpc':'2.0', 'id':'0', 'method':'get_info'}
        headers = {'Content-Type':'application/json'}
        r = requests.post(config.DAEMON_URI + '/json_rpc', json=payload, headers=headers, timeout=5)
        r.raise_for_status()
        j = r.json()
        txs = j['result']['tx_pool_size']
        update.message.reply_text('The current number of transactions in mempool is: {0}'.format(txs))
    except:
        update.message.reply_text('Something borked -_-')

@log_event
@check_debug
def network(update, context):
    try:
        payload = {'jsonrpc':'2.0', 'id':'0', 'method':'get_info'}
        headers = {'Content-Type':'application/json'}
        r = requests.post(config.DAEMON_URI + '/json_rpc', json=payload, headers=headers, timeout=5)
        r.raise_for_status()
        j = r.json()
        height = j['result']['height']
        diff = j['result']['difficulty']
        hashrate = float(diff)/120
        update.message.reply_text('The current block height is {0:,}. Difficulty is {1:,}. Hashrate is {2:.2f} Mh/s.'.format(
            height, diff, hashrate/1e6
        ))
    except:
        update.message.reply_text('Something borked -_-')

@log_event
@check_debug
def price(update, context):
    try:
        data = {
            'localization': False,
            'tickers': False,
            'market_data': True,
            'community_data': False,
            'developer_data': False,
            'sparkline': False
        }
        headers = {'accept': 'application/json'}
        url = 'https://api.coingecko.com/api/v3/coins/wownero'
        r = requests.get(url, headers=headers, data=data, timeout=5)
        r.raise_for_status()
        j = r.json()
        mcap = j['market_cap_rank']
        sats = j['market_data']['current_price']['btc']
        mname = j['tickers'][0]['market']['name']
        volbtc = j['tickers'][0]['converted_volume']['btc']
        tgt = j['tickers'][0]['target']
        bse = j['tickers'][0]['base']
        update.message.reply_text(f'{tgt}-{bse} is {sats:.8f} {tgt} on {mname} with {volbtc:.2f} {tgt} volume. Currently market cap rank #{mcap}.')
    except:
        update.message.reply_text('Something borked -_-')

@log_event
@check_debug
def lastblock(update, context):
    try:
        payload = {'jsonrpc':'2.0', 'id':'0', 'method':'get_last_block_header'}
        headers = {'accept': 'application/json'}
        r = requests.post(config.DAEMON_URI + '/json_rpc', json=payload, headers=headers, timeout=5)
        r.raise_for_status()
        j = r.json()
        block = j['result']['block_header']
        update.message.reply_text('Last block found {0:.2f} minutes ago with height {1} included {2} transactions'.format((
            time() - float(block['timestamp']))/60, block['height'], block['num_txes']
        ))
    except:
        update.message.reply_text('Something borked -_-')
