from tipbot.commands.help import help
from tipbot.commands.debug import debug
from tipbot.commands.register import register
from tipbot.commands.tip import tip
from tipbot.commands.withdraw import withdraw
from tipbot.commands.balance import balance
from tipbot.commands.deposit import deposit
from tipbot.commands.network import mine, mempool, network, price, lastblock


all_commands = {
    'register': {
        'func': register,
        'example': '/register',
        'help': 'Register your Telegram user ID to this bot to begin sending and receiving tips',
    },
    'tip': {
        'func': tip,
        'example': '/tip <username> <amount> <message>',
        'help': 'Tip a user in Wownero'
    },
    'withdraw': {
        'func': withdraw,
        'example': '/withdraw <address> <amount>',
        'help': 'Withdraw Wownero to a specified Wownero address'
    },
    'balance': {
        'func': balance,
        'example': '/balance',
        'help': 'Show your current balance'
    },
    'deposit': {
        'func': deposit,
        'example': '/deposit',
        'help': 'Show your Wownero wallet address for transferring funds to'
    },
    'help': {
        'func': help,
        'example': '/help',
        'help': 'Show available commands for the bot',
    },
    'debug': {
        'func': debug,
        'admin': True
    },
    'mine': {
        'func': mine,
        'example': '/mine <hashes_per_second>',
        'help': 'Determine how much WOW a miner with given hashrate will earn'
    },
    'mempool': {
        'func': mempool,
        'example': '/mempool',
        'help': 'Determine how many transactions currently sit in the mempool'
    },
    'network': {
        'func': network,
        'example': '/network',
        'help': 'Show details about the Wownero network'
    },
    'price': {
        'func': price,
        'example': '/price',
        'help': 'Show Wownero price and market data from CoinGecko'
    },
    'lastblock': {
        'func': lastblock,
        'example': '/lastblock',
        'help': 'Show details about the last processed block on the network'
    }
}
