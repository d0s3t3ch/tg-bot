import logging
from decimal import Decimal
from tipbot import wownero
from tipbot import db
from tipbot.helpers.decorators import wallet_rpc_required, log_event, registration_required, check_debug
from tipbot.helpers.utils import reply_user


@wallet_rpc_required
@registration_required
@log_event
@check_debug
def tip(update, context):
    if len(context.args) < 2:
        _m = 'Not enough arguments passed.'
        reply_user(update.message, context, _m)
        return False
    elif len(context.args) == 2:
        message = ""
    elif len(context.args) > 2:
        message = context.args[2:]

    # validate target user
    if context.args[0].startswith('@'):
        target_un = context.args[0][1:]
    else:
        target_un = context.args[0]

    if target_un == update.message.from_user['first_name']:
        _m = 'You cannot tip yourself!'
        reply_user(update.message, context, _m)
        return False

    if not db.User.filter(telegram_user=target_un):
        reply_text = [
            'That user has not registered and cannot receive tips yet.',
            'If they would like to receive a tip, have them /register with the bot.'
        ]
        _m = ' '.join(reply_text)
        reply_user(update.message, context, _m)
        return False

    # validate amount
    try:
        amount = Decimal(context.args[1])
    except:
        _m = 'Bad Wownero amount specified; not a valid number.'
        reply_user(update.message, context, _m)
        return False

    if amount < 1:
        _m = 'Bad Wownero amount specified. Provide only positive integers or decimals greater than or equal to 1.'
        reply_user(update.message, context, _m)
        return False

    tipper = db.User.get(telegram_id=update.message.from_user['id'])
    tipper_balances = wownero.Wallet().balances(account=tipper.account_index)
    if amount >= tipper_balances[1]:
        _m = f'You do not have sufficient funds to send {amount} WOW. Check your /balance'
        reply_user(update.message, context, _m)
        return False

    # get target user details
    receiver = db.User.get(telegram_user=target_un)
    address = wownero.Wallet().addresses(account=receiver.account_index)[0]

    # transfer funds to user
    try:
        tx = wownero.Wallet().transfer(dest_address=address, amount=wownero.as_wownero(amount), priority=2, account=tipper.account_index)
        if 'tx_hash' in tx:
            h = tx['tx_hash']
            _m = f'Tipped @{target_un} {amount} WOW! Tx: {h}'
            reply_user(update.message, context, _m)
        else:
            logging.error(f'Transaction failure details for {tipper.telegram_user} ({tipper.telegram_id}): {tx}')
            _m = f'Failed to send a tip. Reason: {tx["message"]}'
            reply_user(update.message, context, _m)
    except Exception as e:
        logging.error(f'Unable to send transfer: {e}. Debug: {update.message}')
        _m = 'Failed to send a tip. Ask for help.'
        reply_user(update.message, context, _m)
