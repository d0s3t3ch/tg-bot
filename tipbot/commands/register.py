import logging
from tipbot import wownero
from tipbot.db import User
from tipbot.helpers.decorators import wallet_rpc_required, log_event, check_debug
from tipbot.helpers.utils import reply_user


@wallet_rpc_required
@log_event
@check_debug
def register(update, context):
    wallet = wownero.Wallet()
    msg = update.message
    uid = msg.from_user['id']
    un = getattr(msg.from_user, 'username', None)
    if un is None:
        _m = 'You need a username configured in Telegram to use this bot.'
        reply_user(msg, context, _m)
        return False

    if User.filter(telegram_id=uid):
        if User.filter(telegram_id=uid, telegram_user=un):
            _m = 'You are already registered. Use /help to see available bot commands.'
            reply_user(msg, context, _m)
        else:
            try:
                u = User.get(telegram_id=uid)
                u.telegram_user = un
                u.save()
                _m = f'You have been registered again as Telegram ID {uid} but with username {un}.'
                reply_user(msg, context, _m)
                return True
            except Exception as e:
                logging.error(f'Unable to update user in DB: {e}. Debug: {msg}')
                _m = 'Unable to update your existing account. Ask for help.'
                reply_user(msg, context, _m)
                return False
    else:
        try:
            account = wallet.new_account(label=un)
        except Exception as e:
            logging.error(f'Unable to create a new account in wallet RPC: {e}. Debug: {msg}')
            _m = 'Unable to create a new account for you. Ask for help.'
            reply_user(msg, context, _m)
            return False
        try:
            u = User(
                telegram_id=uid,
                telegram_user=un,
                account_index=account[0],
                address=account[1]
            )
            u.save()
            reply_text = [
                f'You have been registered as Telegram ID {uid} and username {un} and can now send and receive tips.',
                'Ask for /help to see all available bot commands. Maybe start with /deposit to get your deposit address.'
            ]
            _m = ' '.join(reply_text)
            reply_user(msg, context, _m)
            return True
        except Exception as e:
            logging.error(f'Unable to register user in DB: {e}. Debug: {msg}')
            _m = 'Unable to create a new account for you. Ask for help.'
            reply_user(msg, context, _m)
            return False
