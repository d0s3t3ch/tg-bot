import logging
from tipbot import wownero
from tipbot import db
from tipbot.helpers.decorators import wallet_rpc_required, log_event, registration_required, check_debug
from tipbot.helpers.utils import reply_user


@wallet_rpc_required
@registration_required
@log_event
@check_debug
def balance(update, context):
    u = db.User.get(telegram_id=update.message.from_user.id)
    balances = wownero.Wallet().balances(account=u.account_index)
    unlocked = float(balances[1])
    locked = float(balances[0] - balances[1])
    msg = f'Available balance for {u.telegram_user}: {unlocked} WOW ({locked} WOW locked)'
    reply_user(update.message, context, msg)
