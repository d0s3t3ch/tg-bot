import logging
from decimal import Decimal
from tipbot import wownero
from tipbot import db
from tipbot.helpers.decorators import wallet_rpc_required, log_event, registration_required, check_debug
from tipbot.helpers.utils import reply_user


@wallet_rpc_required
@registration_required
@log_event
@check_debug
def withdraw(update, context):
    if len(context.args) < 2:
        _m = 'Not enough arguments passed.'
        reply_user(update.message, context, _m)
        return False

    # validate address
    if len(context.args[0]) in [97, 108]:
        address = context.args[0]
    else:
        _m = 'This does not look like a valid Wownero address. Try again.'
        reply_user(update.message, context, _m)
        return False

    # validate amount
    try:
        amount = Decimal(context.args[1])
    except:
        _m = 'Bad Wownero amount specified; not a valid number.'
        reply_user(update.message, context, _m)
        return False

    if amount < 1:
        _m = 'Bad Wownero amount specified. Provide only positive integers or decimals greater than or equal to 1.'
        reply_user(update.message, context, _m)
        return False

    sender = db.User.get(telegram_id=update.message.from_user.id)
    sender_balances = wownero.Wallet().balances(account=sender.account_index)
    if amount > sender_balances[1]:
        _m = f'You do not have sufficient funds to withdraw {amount} WOW. Check your /balance'
        reply_user(update.message, context, _m)
        return False

    # transfer funds to given address
    try:
        tx = wownero.Wallet().transfer(dest_address=address, amount=wownero.as_wownero(amount), priority=2, account=sender.account_index)
        if 'tx_hash' in tx:
            h = tx['tx_hash']
            _m = f'Sent {amount} WOW! Tx: {h}'
            reply_user(update.message, context, _m)
        else:
            logging.error(f'Transaction failure details for {sender.telegram_user} ({sender.telegram_id}): {tx}')
            _m = f'Failed to withdraw Wownero. Reason: "{tx["message"]}"'
            reply_user(update.message, context, _m)
    except Exception as e:
        logging.error(f'Unable to withdraw transfer: {e}. Debug: {update.message}')
        _m = 'Failed to withdraw Wownero. Ask for help.'
        reply_user(update.message, context, _m)
