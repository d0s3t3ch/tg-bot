from tipbot import commands
from tipbot.helpers.utils import reply_user


def help(update, context):
    cmds = list()
    for i in commands.all_commands:
        pk = commands.all_commands[i]
        if not pk.get('admin', False):
            cmds.append('{example} - {help}'.format(
                example=pk['example'],
                help=pk['help']
            ))
    msg = 'Here are the available commands for this bot:\n\n' + '\n\n'.join(cmds)
    reply_user(update.message, context, msg)
