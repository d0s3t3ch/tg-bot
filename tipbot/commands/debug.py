from tipbot.helpers.decorators import wallet_rpc_required, log_event, check_debug
from tipbot.helpers.utils import is_tg_admin
from tipbot.wownero import Wallet


@wallet_rpc_required
@log_event
@check_debug
def debug(update, context):
    if is_tg_admin(update.message.from_user.id):
        update.message.reply_text('ohai')
    else:
        update.message.reply_text('you cant do that.')
