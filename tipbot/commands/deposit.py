import logging
from telegram import ParseMode
from telegram.error import Unauthorized
from tipbot import wownero
from tipbot import db
from tipbot.helpers.decorators import wallet_rpc_required, log_event, registration_required, check_debug
from tipbot.helpers.utils import generate_qr


@wallet_rpc_required
@registration_required
@log_event
@check_debug
def deposit(update, context):
    u = db.User.get(telegram_id=update.message.from_user.id)
    address = wownero.Wallet().addresses(account=u.account_index)[0]
    try:
        update.message.from_user.send_photo(
            photo=generate_qr(address),
            caption=f'{u.telegram_user}\'s deposit address: {address}',
            quote=False
        )
        update.message.delete()
    except Unauthorized:
        update.message.reply_text(f'You have to initiate a convo with the bot first: https://t.me/{context.bot.username}')
        update.message.delete()
    except:
        update.message.reply_text(f'Something borked -_-')
        update.message.delete()
