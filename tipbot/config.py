from dotenv import load_dotenv
from os import getenv

load_dotenv()

TG_TOKEN = getenv('TG_TOKEN')
TG_ADMIN_ID = int(getenv('TG_ADMIN_ID'))
WALLET_RPC_USER = getenv('WALLET_RPC_USER')
WALLET_RPC_PASS = getenv('WALLET_RPC_PASS')
SQLITE_DB_PATH = getenv('SQLITE_DB_PATH')
DAEMON_URI = getenv('DAEMON_URI')
_d = getenv('DEBUG', True)

if isinstance(_d, str):
    if _d.lower() == 'false':
        DEBUG = False
    else:
        DEBUG = True
