from peewee import *
from tipbot import config


db = SqliteDatabase(config.SQLITE_DB_PATH)

class BaseModel(Model):
    class Meta:
        database = db

class User(BaseModel):
    telegram_id = IntegerField(unique=True)
    telegram_user = CharField(unique=True)
    account_index = IntegerField(unique=True)
    address = CharField(unique=True)
