import click
from tipbot import db
from tipbot import wownero
from tipbot import commands


@click.group()
def cli():
    pass

@click.command()
def debug():
    pass

@click.command()
def get_users():
    users = db.User.select()
    wallet = wownero.Wallet()
    for u in users:
        click.echo(f'{u.telegram_user} ({u.telegram_id}) - wallet {u.account_index} ({u.address[0:12]})')

@click.command()
@click.argument('account_index')
def get_address(account_index):
    address = wownero.Wallet().addresses(account=int(account_index))[0]
    click.echo(address)

@click.command()
def get_balances():
    wallet = wownero.Wallet()
    accounts = wallet.accounts()
    for acc in accounts:
        balances = wallet.balances(account=acc[0])
        unlocked = balances[1]
        locked = balances[0] - balances[1]
        click.echo(f'wallet {acc[0]} ({acc[1][0:12]}) - {float(locked)} locked, {float(unlocked)} unlocked')

@click.command()
def generate_bot_help():
    for cmd in commands.all_commands:
        c = commands.all_commands[cmd]
        if not 'admin' in c:
            click.echo(f'{cmd} - {commands.all_commands[cmd]["help"]}')

@click.command()
def db_init():
    try:
        db.db.create_tables([db.User])
        click.echo('Success')
    except:
        click.echo('Fail')

cli.add_command(debug)
cli.add_command(get_users)
cli.add_command(get_address)
cli.add_command(generate_bot_help)
cli.add_command(get_balances)
cli.add_command(db_init)

if __name__ == '__main__':
    cli()
