setup:
	python3 -m venv .venv
	.venv/bin/pip install -r requirements.txt

up:
	.venv/bin/python3 -m tipbot
